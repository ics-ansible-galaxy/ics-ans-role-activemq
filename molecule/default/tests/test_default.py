import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_activemq_running_and_enabled(host):
    activemq = host.service('activemq.service')
    assert activemq.is_running
    assert activemq.is_enabled


def test_activemq_users(host):
    users = host.file('/opt/activemq/conf/users.properties')
    assert users.contains('admin=adminpwd')
    assert users.contains('user=userpwd')
    assert users.user == 'activemq'
    assert users.group == 'activemq'
    assert users.mode == 0o640


def test_activemq_groups(host):
    groups = host.file('/opt/activemq/conf/groups.properties')
    assert groups.contains('admins=admin,user')
    assert groups.user == 'activemq'
    assert groups.group == 'activemq'
    assert groups.mode == 0o640
